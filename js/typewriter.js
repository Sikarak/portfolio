document.addEventListener('DOMContentLoaded',function(event){

  var dataText = [ "Raphaël Vareilles.", "a front end developer.","senior webdesigner.","an HTML / CSS Expert.", "senior graphic designer.", "a gamedeveloper too.", "in love with sass.", "UX / UI Sensible.", "using jquery.", "making games sometimes."];

  function typeWriter(text, i, fnCallback) {
    // chekc if text isn't finished yet
    if (i < (text.length)) {
      // add next character to h1
     document.querySelector(".writer").innerHTML = text.substring(0, i+1) +'<span aria-hidden="true"></span>';

      // wait for a while and call this function again for next character
      setTimeout(function() {
        typeWriter(text, i + 1, fnCallback)
      }, 100);
    }
    // text finished, call callback if there is a callback function
    else if (typeof fnCallback == 'function') {
      // call callback after timeout
      setTimeout(fnCallback, 1800);
    }
  }

  function randomIntFromInterval(min,max)
    {
        return Math.floor(Math.random()*(max-min+1)+min);
    }

   function StartTextAnimation(i) {
     if (typeof dataText[i] != 'undefined'){
        setTimeout(function() {
                // check if dataText[i] exists
                if (i < dataText.length) {
                  // text exists! start typewriter animation
                 typeWriter(dataText[i], 0, function(){
                   // after callback (and whole text has been animated), start next text
                   StartTextAnimation(randomIntFromInterval(0,dataText.length -1));
                 });
                }
        }, 2000);
     }
  }
  // start the text animation
  StartTextAnimation(0);
});
