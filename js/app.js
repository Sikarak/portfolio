//Angular
 angular.module("portfolioApp", [])

.controller('MainController',  function($scope) {

    $scope.obj = [
        {
            // Eurotunnel
            thumb: 'img/projects/thumbs/eurotunnel.png',
            head: 'intranet',
            cover: 'img/projects/eurotunnel/cover.png',
            title: 'Intranet Eurotunnel',
            subtitle: 'Design d’espace profesionnel',
            description: 'Lancement d\'activité, conception de l\'identité visuelle et du site web.',
            pictures: [
                {
                    thumb: 'img/projects/eurotunnel/pic1.png',
                    big: 'img/projects/eurotunnel/pic1-big.jpg',
                    id: '#pic1'
                 },
                {
                    thumb: 'img/projects/eurotunnel/pic2.png',
                    big: 'img/projects/eurotunnel/pic2-big.jpg',
                    id: '#pic2'
                },
                {
                    thumb: 'img/projects/eurotunnel/pic3.png',
                    big: 'img/projects/eurotunnel/pic3-big.png',
                    id: '#pic3'
                },
            ],
            table1: [
                'Axure',
                'Photoshop',
                'Bootstrap 4',
                'Sharepoint'],
            table2: [
                {text: 'www.eurotunnel.fr', url: 'http://www.eurotunnel.fr'}
            ],
            tags: ['design', 'intégration']
        },
        {
            // Du côté des restos
            thumb: 'img/projects/thumbs/up.png',
            head: 'e-commerce',
            cover: 'img/projects/up/cover.png',
            title: 'Du côté des restos',
            subtitle: 'Boutique pour restaurateurs',
            description: 'Lancement d\'activité, conception de l\'identité visuelle et du site web.',
            pictures: [
                {
                    thumb: 'img/projects/up/pic1.png',
                    big: 'img/projects/up/pic1-big.jpg',
                    id: '#pic1'
                },
                {
                    thumb: 'img/projects/up/pic2.png',
                    big: 'img/projects/up/pic2-big.jpg',
                    id: '#pic2'
                },
                {
                    thumb: 'img/projects/up/pic3.png',
                    big: 'img/projects/up/pic3-big.jpg',
                    id: '#pic3'
                },
            ],
            table1: [
                'Axure',
                'Photoshop',
                'Bootstrap 3',
                'Liferay'],
            table2: [
                {text: 'www.ducotedesrestos.fr', url: 'http://www.ducotedesrestos.fr'}
            ],
            tags: ['design', 'intégration', 'toto', 'titi', 'tatatata', 'popopopo']
        },
        {
            // Bel intranet
            thumb: 'img/projects/thumbs/bel.png',
            head: 'intranet',
            cover: 'img/projects/bel/cover.png',
            title: 'Intranet groupe Bel',
            subtitle: 'Boutique pour restaurateurs',
            description: 'Lancement d\'activité, conception de l\'identité visuelle et du site web.',
            pictures: [
                {
                    thumb: 'img/projects/bel/pic1.png',
                    big: 'img/projects/bel/pic1-big.jpg',
                    id: '#pic1'
                },
                {
                    thumb: 'img/projects/bel/pic2.png',
                    big: 'img/projects/bel/pic2-big.jpg',
                    id: '#pic2'
                },
                {
                    thumb: 'img/projects/bel/pic3.png',
                    big: 'img/projects/bel/pic3-big.jpg',
                    id: '#pic3'
                },
                {
                    thumb: 'img/projects/bel/pic4.png',
                    big: 'img/projects/bel/pic4-big.jpg',
                    id: '#pic4'
                },
                {
                    thumb: 'img/projects/bel/pic5.png',
                    big: 'img/projects/bel/pic5-big.jpg',
                    id: '#pic5'
                },
                {
                    thumb: 'img/projects/bel/pic6.png',
                    big: 'img/projects/bel/pic6-big.jpg',
                    id: '#pic6'
                },
            ],
            table1: [
                'Axure',
                'Photoshop',
                'Bootstrap 3',
                'Liferay'],
            table2: [
                {text: 'www.groupebel.fr', url: 'http://www.groupebel.fr'}
            ],
            tags: ['design', 'intégration', 'toto', 'titi', 'tatatata', 'popopopo']
        },
        {
            // Fidal
            thumb: 'img/projects/thumbs/fidal.png',
            head: 'intranet',
            cover: 'img/projects/fidal/cover.png',
            title: 'Intranet Fidal',
            subtitle: 'Design d\'espace professionnel',
            description: 'Lancement d\'activité, conception de l\'identité visuelle et du site web.',
            thumbs: [
            'img/projects/fidal/pic1.png',
            'img/projects/fidal/pic2.png',
            'img/projects/fidal/pic3.png',
            ],
            table1: [
            'Axure',
            'Photoshop',
            'Bootstrap 3',
            'Liferay'],
            table2: [
                {text: 'www.fidal.fr', url: 'http://www.fidal.fr'}
            ],
            tags: ['design', 'intégration', 'toto', 'titi', 'tatatata', 'popopopo']
        },
        {
            // Seloger
            thumb: 'img/projects/thumbs/seloger.png',
            head: 'intranet',
            cover: 'img/projects/seloger/cover.png',
            title: 'Cartographie SeLoger.com',
            subtitle: 'UX / UI + design applicatif. Intégration',
            description: 'Design et intégration du nouveau module de cartographie.',
            thumbs: [
                'img/projects/seloger/pic1.png',
                'img/projects/seloger/pic2.png',
                'img/projects/seloger/pic3.png',
            ],
            table1: [
                'Axure',
                'Photoshop',
                'Bootstrap 3',
                'Liferay'],
            table2: [
                {text: 'www.seloger.com', url: 'http://www.seloger.com'}
            ],
            tags: ['design', 'intégration', 'toto', 'titi', 'tatatata', 'popopopo']
        },
        {
            // Convertigo
            thumb: 'img/projects/thumbs/convertigo.png',
            head: 'intranet',
            cover: 'img/projects/convertigo/cover.png',
            title: 'Convertigo',
            subtitle: 'UX / UI + design applicatif. Intégration',
            description: 'A remplir',
            thumbs: [
                'img/projects/convertigo/pic1.png',
                'img/projects/convertigo/pic2.png',
                'img/projects/convertigo/pic3.png',
            ],
            table1: [
                'Axure',
                'Photoshop',
                'Bootstrap 3',
                'Liferay'],
            table2: [
                {text: 'www.convertigo.com', url: 'http://www.convertigo.com'}
            ],
            tags: ['design', 'intégration', 'toto', 'titi', 'tatatata', 'popopopo']
        },
    ]


     $scope.style = {};

    $scope.pop = function(div){
        $scope.style = div;
    }
})


//Foundation
$(document).foundation();

//Particules
particlesJS('particles-js',

  {
    "particles": {
      "number": {
        "value": 80,
        "density": {
          "enable": true,
          "value_area": 800
        }
      },
      "color": {
        "value": "#000"
      },
      "shape": {
        "type": "circle",
        "stroke": {
          "width": 0,
          "color": "#000000"
        },
        "polygon": {
          "nb_sides": 5
        },
        "image": {
          "src": "img/github.svg",
          "width": 100,
          "height": 100
        }
      },
      "opacity": {
        "value": 0.5,
        "random": false,
        "anim": {
          "enable": false,
          "speed": 1,
          "opacity_min": 0.1,
          "sync": false
        }
      },
      "size": {
        "value": 5,
        "random": true,
        "anim": {
          "enable": false,
          "speed": 40,
          "size_min": 0.1,
          "sync": false
        }
      },
      "line_linked": {
        "enable": true,
        "distance": 150,
        "color": "#000",
        "opacity": 0.4,
        "width": 1
      },
      "move": {
        "enable": true,
        "speed": 6,
        "direction": "none",
        "random": false,
        "straight": false,
        "out_mode": "out",
        "attract": {
          "enable": false,
          "rotateX": 600,
          "rotateY": 1200
        }
      }
    },
    "interactivity": {
      "detect_on": "canvas",
      "events": {
        "onhover": {
          "enable": true,
          "mode": "repulse"
        },
        "onclick": {
          "enable": true,
          "mode": "push"
        },
        "resize": true
      },
      "modes": {
        "grab": {
          "distance": 400,
          "line_linked": {
            "opacity": 1
          }
        },
        "bubble": {
          "distance": 400,
          "size": 40,
          "duration": 2,
          "opacity": 8,
          "speed": 3
        },
        "repulse": {
          "distance": 200
        },
        "push": {
          "particles_nb": 4
        },
        "remove": {
          "particles_nb": 2
        }
      }
    },
    "retina_detect": true,
    "config_demo": {
      "hide_card": false,
      "background_color": "#b61924",
      "background_image": "",
      "background_position": "50% 50%",
      "background_repeat": "no-repeat",
      "background_size": "cover"
    }
  }

);


