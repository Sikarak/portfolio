$(document).ready(function() {

	//// logo

	$('.logo-cont').hover(function() {
		$('.logo').addClass('bounce');
    }, function(){
    $('.logo').removeClass('bounce');
	});

	//// icons

	$('#portfolio').hover(function(){
		$('.portfolio-icon').css('backgroundImage', 'url(./img/nav/portfolio-on.svg)').addClass('swing');
    }, function(){
    $('.portfolio-icon').css('backgroundImage', 'url(./img/nav/portfolio-off.svg)').removeClass('swing');
	});

	$('#profil').hover(function(){
		$('.profil-icon').css('backgroundImage', 'url(./img/nav/profil-on.svg)').addClass('tada');
    }, function(){
    $('.profil-icon').css('backgroundImage', 'url(./img/nav/profil-off.svg)').removeClass('tada');
	});

	$('#contact').hover(function(){
		$('.contact-icon').css('backgroundImage', 'url(./img/nav/contact-on.svg)').addClass('jello');
    }, function(){
    $('.contact-icon').css('backgroundImage', 'url(./img/nav/contact-off.svg)').removeClass('jello');
	});

	//// social-module

	$('#linkedin').hover(

		function(){
		$(this).css('background-color', '#0077B5')
		$('.linkedin-icon').css('backgroundImage', 'url(./img/nav/social-module/linkedin-on.svg)').addClass('');
    }, function(){
		$(this).css('background-color', '#1f1f1f')
    $('.linkedin-icon').css('backgroundImage', 'url(./img/nav/social-module/linkedin-off.svg)').removeClass('');
	});

	$('#twitter').hover(function(){
		$(this).css('background-color', '#00aced')
		$('.twitter-icon').css('backgroundImage', 'url(./img/nav/social-module/twitter-on.svg)').addClass('');
    }, function(){
		$(this).css('background-color', '#1f1f1f')
    $('.twitter-icon').css('backgroundImage', 'url(./img/nav/social-module/twitter-off.svg)').removeClass('');
	});

	$('#facebook').hover(function(){
		$(this).css('background-color', '#3b5998')
		$('.facebook-icon').css('backgroundImage', 'url(./img/nav/social-module/facebook-on.svg)').addClass('');
    }, function(){
		$(this).css('background-color', '#1f1f1f')
    $('.facebook-icon').css('backgroundImage', 'url(./img/nav/social-module/facebook-off.svg)').removeClass('');
	});

	// --> social-module : interactions

	$('.social-module').hover(function(){

		$('.social-separator').fadeToggle(700);
		$('#linkedin').fadeToggle(500);
		$('#twitter').fadeToggle(300);
		$('#facebook').fadeToggle(100);



		$('#share').css('background-color', '#0f0f0f');
		$('.share-icon').addClass('bounceIn');
    }, function(){

		$('.social-separator').fadeToggle(1);
		$('#linkedin').fadeToggle(100);
		$('#twitter').fadeToggle(300);
		$('#facebook').fadeToggle(600);

		$('#share').css('background-color', '#1f1f1f');
		$('.share-icon').removeClass('bounceIn');
	});




});
