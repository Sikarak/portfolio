$(document).ready(function() {

    var popIn = $('.pop-in');
    var close = $('.close-button');
    var cover = $('.cover');

    //descriptions
    var title = $('.titre');
    var subtitle = $('.sous-titre');
    var mission =  $('.description');
    var details = $('.details');

    $('.info, .desc, .icon-eye, .thumb').click(function() {
        popIn.removeClass('slideOutUp');
        popIn.addClass('slideInDown');
        popIn.css('display', 'block');
        cover.addClass('bounceIn');
        title.addClass('fadeInDown');
        subtitle.addClass('fadeInDown');
        mission.addClass('fadeInLeft');
        details.addClass('fadeInUp');
    });

    // CLOSE WINDOW
    close.hover(function() {
        $(this).addClass('rotateIn');
    },
               function() {
        $(this).removeClass('rotateIn');
    });

    close.click(function() {
        popIn.removeClass('slideInDown');
        popIn.addClass('slideOutUp');
        cover.removeClass('bounceIn');
        title.removeClass('fadeInDown');
        subtitle.removeClass('fadeInDown');
        mission.removeClass('fadeInLeft');
        details.removeClass('fadeInUp');
    });



});


